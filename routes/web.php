<?php

// use  App\Http\Controllers\ProductController;

use App\Http\Controllers\AdminController;
use  App\Http\Controllers\AuthController;
use  App\Http\Controllers\PublicController;
use  App\Http\Controllers\DashBoardController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/', 'homepage');

// Route::get('/about', [PublicController::class,'about']);


// Route::get('/register', [AuthController::class,'registration']);
// Route::get('/login', [AuthController::class,'login']);

Route::get('/home', [DashBoardController::class,'ShowHome'])->name('home');
Route::get('/login', [DashBoardController::class,'login']);

Route::get('/shop-classic-filter', [DashBoardController::class,'classicfilter']);











// Route::get('/index', [ProductController::class,'index']);
// Route::get('/login', [AuthController::class,'login']);
// Route::get('/registration', [AuthController::class,'registration']);
// Route::get('/about', [PublicController::class,'about']);
// Route::get('/dashboard', [DashBoardController::class,'dashboard']);